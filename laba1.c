#include <stdio.h>

double f(double e1, double e2, double e3) {
    int n = 1;
    double m = e1;
    if (e2 < m) {
        n = 2;
        m = e2;
    }
    if (e3 < m) {
        n = 3;
        m = e3;
    }
    double s = e1 + e2 + e3;
    if (s > 0)
        return n / s;
    else
        return s / n;
}

int main(void) {
    double e1, e2, e3;
    printf("e1 = ");
    scanf("%lf", &e1);
    printf("e2 = ");
    scanf("%lf", &e2);
    printf("e3 = ");
    scanf("%lf", &e3);
    printf("Result: %f\n", f(e1, e2, e3));
    return 0;
}